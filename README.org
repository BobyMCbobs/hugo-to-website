#+TITLE: Hugo to website

#+begin_quote
A repo for CI configurations to deploy a standard Hugo website in various ways
#+end_quote

* Background
Is it common to want to abstract away the infrastucture for a website but still to want to care about the management of a website.
This repo provides multiple GitLab CI configuration files to import and use in your repo.

* Solutions
To set up a solution up in your repo, navigate to /your repo/ -> /Settings/ -> /CI/CD/ -> /General pipelines/, set the field of /CI/CD configuration file/ to one of the following values.
To include a solution in an existing repo, in your /.gitlab-ci.yml/, write

#+begin_src yaml
include:
  - remote: https://gitlab.com/BobyMCbobs/hugo-to-website/-/raw/main/SOMETHING.yml
#+end_src

replacing the three dots with the name of the file

** GitLab Pages
The URL for the configuration is [[https://gitlab.com/BobyMCbobs/hugo-to-website/-/raw/main/gitlab-ci-gitlab-pages.yml]]

*** Configuration
After importing or setting the configuration, see the GitLab pages configuration at https://gitlab.com/[ORG]/[PROJECT]/pages. In this section, you can set a domain and HTTPS settings.
The base URL is overridable with the =$CUSTOM_PAGES_URL= variable.

** TODO Kubernetes (coming soon)
The URL for the configuration is [[https://gitlab.com/BobyMCbobs/hugo-to-website/-/raw/main/gitlab-ci-kubernetes.yml]]

** TODO Knative (coming soon)
The URL for the configuration is [[https://gitlab.com/BobyMCbobs/hugo-to-website/-/raw/main/gitlab-ci-knative.yml]]

** Omnibus
The URL for the configuration is [[https://gitlab.com/BobyMCbobs/hugo-to-website/-/raw/main/gitlab-ci-omnibus.yml]]

This configuration file conditionally includes all above solutions. To choose a solution, the varable =$HUGO_TO_WEBSITE= must be set to one of the following
- gitlab-pages
- kubernetes
- knative
